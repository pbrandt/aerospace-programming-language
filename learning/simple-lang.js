// Generated automatically by nearley, version 2.19.3
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

const lexer = require('../src/tokenizer').lexer
const memory = {}
var grammar = {
    Lexer: lexer,
    ParserRules: [
    {"name": "main", "symbols": ["statementlist"]},
    {"name": "statementlist$ebnf$1", "symbols": []},
    {"name": "statementlist$ebnf$1", "symbols": ["statementlist$ebnf$1", "statement"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "statementlist", "symbols": ["statementlist$ebnf$1"]},
    {"name": "statement", "symbols": [(lexer.has("symbol") ? {type: "symbol"} : symbol), (lexer.has("equals") ? {type: "equals"} : equals), "AS", "semicolon"], "postprocess":  (d) => {
            var s = d[0]
            console.log(`setting ${s} = ${d[2]}`)
            memory[s] = d[2]
            return `${s} = ${d[2]}`
        }},
    {"name": "statement", "symbols": ["AS"], "postprocess":  d => {
            console.log(d[0])
            return `console.log(${d[0]})`
        }},
    {"name": "P", "symbols": [(lexer.has("lparen") ? {type: "lparen"} : lparen), "AS", (lexer.has("rparen") ? {type: "rparen"} : rparen)], "postprocess": d => d[1]},
    {"name": "P", "symbols": ["N"], "postprocess": id},
    {"name": "E", "symbols": ["P", (lexer.has("caret") ? {type: "caret"} : caret), "E"], "postprocess": d => Math.pow(d[0], d[2])},
    {"name": "E", "symbols": ["P"], "postprocess": id},
    {"name": "MD", "symbols": ["MD", (lexer.has("times") ? {type: "times"} : times), "E"], "postprocess": d => d[0] * d[2]},
    {"name": "MD", "symbols": ["MD", (lexer.has("divide") ? {type: "divide"} : divide), "E"], "postprocess": d => d[0] / d[2]},
    {"name": "MD", "symbols": ["E"], "postprocess": id},
    {"name": "AS", "symbols": ["AS", (lexer.has("plus") ? {type: "plus"} : plus), "MD"], "postprocess": (d) => d[0] + d[2]},
    {"name": "AS", "symbols": ["AS", (lexer.has("minus") ? {type: "minus"} : minus), "MD"], "postprocess": d => d[0] - d[2]},
    {"name": "AS", "symbols": ["MD"], "postprocess": id},
    {"name": "N", "symbols": [(lexer.has("int") ? {type: "int"} : int)], "postprocess": d => parseInt(d[0])},
    {"name": "N", "symbols": [(lexer.has("float") ? {type: "float"} : float)], "postprocess": d => parseFloat(d[0])},
    {"name": "N", "symbols": [(lexer.has("symbol") ? {type: "symbol"} : symbol)], "postprocess": d => memory[d[0]]},
    {"name": "semicolon", "symbols": [(lexer.has("semicolon") ? {type: "semicolon"} : semicolon)], "postprocess": () => null}
]
  , ParserStart: "main"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
})();
