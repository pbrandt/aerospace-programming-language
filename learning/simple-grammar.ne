# wrote a tokenizer/lexer using moo.js
@{%
const lexer = require('../src/tokenizer').lexer
const memory = {}
%}
@lexer lexer
main ->
    statementlist

statementlist ->
    statement:*

statement ->
    %symbol %equals AS semicolon {% (d) => {
        var s = d[0]
        console.log(`setting ${s} = ${d[2]}`)
        memory[s] = d[2]
        return `${s} = ${d[2]}`
    }%}
    | AS {% d => {
        console.log(d[0])
        return `console.log(${d[0]})`
    }%}

P ->
    %lparen AS %rparen {% d => d[1] %}
    | N {% id %}

E -> P %caret E {% d => Math.pow(d[0], d[2]) %}
    | P {% id %}

MD -> MD %times E {% d => d[0] * d[2] %}
    | MD %divide E {% d => d[0] / d[2] %}
    | E {% id %}

AS ->
    AS %plus MD {% (d) => d[0] + d[2] %}
    | AS %minus MD {% d => d[0] - d[2] %}
    | MD {% id %}

N ->
    %int {% d => parseInt(d[0]) %}
    | %float {% d => parseFloat(d[0]) %}
    | %symbol {% d => memory[d[0]] %}

semicolon -> %semicolon {% () => null %}
