#!/usr/bin/env bash

node ../src/tokenizer.js simple-test.s
nearleyc -o simple-lang.js simple-grammar.ne
nearley-test simple-lang.js < simple-test.s

