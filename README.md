# Aerospace programming language

This is a **modeling language**. You would use it when you need to model a physical process, like the flight of a spacecraft to the moon.

There's two main prongs of development here:

1. A base language suited for modeling physics, including primitives for units like meters
2. A large standard library to enable separately developed programs to interact efficiently, containing standard reference frames, time systems, and ephemerides.

- Sane module exports
- `async/await`
- `multiprocessing`
- `cluster` for communicating between different computers
- Quantity types with unit conversion
- Matrix types and linear algebra built-in
- Reference frames conversions in the standard library
- Timekeeping system conversions in the standard library
- Fractional types ?
- Symbolic math ?
- Complex types ?
- Physical state types

## Examples

```c
i32 i = 0;
f64 foo = 1.25;
frac bar = 5/4;
q64 height = 1.25 m;
utc launch = 2020-08-11 14:23:00.000Z;
tdb orbit_insertion = launch + 345 s;
f64 A = [ 1 1; -1 0 ]; // semi colons separate rows
f64 x = [ 2; 3 ]; // knows dimensions when declared and assigned inline
f64 b = A * x;
f64 H [2,2]; // can also declare dimensions without assigning
H = [1 0; 0, 1]; // nobody cares if you use commas or not
q64 positions [] m; // variable-length array where each element has unit m
q64 state [m ; m/s] // 2x1 matrix/vector where the first element has unit m and second m/s
typedef state = q64 [m ; m/s]; // typedefs are a thing
state states[]; // array of state types

// structs are not vectors
// they can be nested
struct state = {
  x: q64 m;
  y: q64 m;
  vx: q64 m/s;
  vy: q64 m/s;
  properties: {
    drag_area: q64 m^2;
    drag_coefficient: f64;
  }
};

// vectors are vectors
vector state = [
  x: q64 m;
  y: q64 m;
  vx: q64 m/s;
  vy: q64 m/s;
];

// physical states are first-class objects
position p m; // same as q64 p[3] m;
velocity v km/s; // same as q64 v[3] kms;

// directions are default in an inertial frame
// directions are unitless
direction sun = [1, 0, 0];
direction earth moon_earth_rotating = [1, 0, 0];

// by default they are in an inertial frame
// but you can put them in a custom frame
position sc_position m earth_j2k = [6378 + 400, 0, 0]


```

## How the units stuff works

```
q64 foo = 1 m;
q64 bar = 2 km;
q64 baz = 3 cm;
q64 volume = foo * bar * baz; // read below for what this is
```

1. the compiler knows that volume will have units of length^3. the compiler always gives precedence to the leftmost unit, so in this case it will have base units m^3.
2. to calculate the numeric value of `volume` the compiler will insert constants

converted to c:

```c
double volume = foo * (bar * 1e3) * (baz * 1e-2);
```

which gets optimized to:

```c
double volume = foo * bar * baz * 1e1;
```


## modeling

modeling is done via tables which have a very strict memory layout. it's literally sqlite under the hood, which allows you to index stuff.

```
define table foo {
  q64 bar km not null default 0;
  q64 baz s not null defautl 0;
}

table foos = foo[100];
table more_foos = foo[1000];
```

internally this gets compiled to something like this:

```c
struct foo_definition {...}; // contains unit conversion stuf

struct foos {
  double bar[100];
  double baz[100];
}
struct more_foos {
  double bar[1000];
  double baz[1000];
}
```

you can compose tables, adding the above `foo` to another table.

```
define table 2dfoo {
  foo a;
  foo b;
  q64 dist m;
}
table 2dfoos = 2dfoo[100];
```

pointers are not used under the hood. all of this gets its own row like this:

```c
struct 2dfoo_definition {...}; // contains unit conversion stuff

struct 2dfoos {
  double a_bar[100];
  double a_baz[100];
  double b_bar[100];
  double b_baz[100];
  double dist[100];
}
```

for computation we use something like the entity component system way of doing things. the following example illustrates using a component to define motion.


```
define table pendulum {
  q64 theta deg not null default 0;
  q64 thetadot deg/s not null default 0;
  q64 l m not null default 1;
  q64 m kg not null default 1;
  q64 time s not null default 0;
}

// because tables can be used like units, you can do stuff like this:
define table pendulum_derivatives = pendulum / s;

// which is IDENTICAL to the following
define table pendulum_derivatvies {
  q64 theta deg/s not null default 0;
  q64 thetadot deg/s^2 not null default 0;
  q64 l m/s not null default 1;
  q64 m kg/s not null default 1;
  q64 time s/s not null default 0;
}

// and since you can compose units anywhere, you don't even need to define pendulum_derivatives
// in order to make a derivative component for pendulum
define component derivative<unit u> (t) -> (t/u);

// you can implement this component for anything
implement derivative<s> for pendulum (state) -> (deriv) {
  q64 g = 9.80665 m/s^2;
  deriv.theta = state.thetadot;
  deriv.thetadot = -1 * g / state.l * sin(state.theta);
  deriv.l = 0;
  deriv.m = 0;
  deriv.time = 1;
}

pendulum p;

while (p.time < 100) {
  p = p + derivative<s>(p) * 0.1 s;
  debug(p)
}

```


## C interop

because of the relatively simple memory model, i think it might be possible to export c header files from the compilation process and link with a c program.


