// Generated automatically by nearley, version 2.19.3
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

const lexer = require('./src/tokenizer').lexer
var grammar = {
    Lexer: lexer,
    ParserRules: [
    {"name": "main", "symbols": ["statementlist"]},
    {"name": "statementlist$ebnf$1", "symbols": []},
    {"name": "statementlist$ebnf$1", "symbols": ["statementlist$ebnf$1", "statement"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "statementlist", "symbols": ["statementlist$ebnf$1"]},
    {"name": "statement", "symbols": [(lexer.has("semicolon") ? {type: "semicolon"} : semicolon)]},
    {"name": "statement$subexpression$1", "symbols": ["declaration"]},
    {"name": "statement$subexpression$1", "symbols": ["declareassign"]},
    {"name": "statement$subexpression$1", "symbols": ["expr"]},
    {"name": "statement", "symbols": ["statement$subexpression$1", (lexer.has("semicolon") ? {type: "semicolon"} : semicolon)]},
    {"name": "declaration", "symbols": ["definition"]},
    {"name": "declareassign", "symbols": ["definition", (lexer.has("equals") ? {type: "equals"} : equals), "expr"]},
    {"name": "definition", "symbols": [(lexer.has("type") ? {type: "type"} : type), (lexer.has("symbol") ? {type: "symbol"} : symbol)]},
    {"name": "expr$subexpression$1", "symbols": ["math"]},
    {"name": "expr$subexpression$1", "symbols": ["call"]},
    {"name": "expr", "symbols": ["expr$subexpression$1"]},
    {"name": "call", "symbols": [(lexer.has("symbol") ? {type: "symbol"} : symbol), (lexer.has("lparen") ? {type: "lparen"} : lparen), (lexer.has("symbol") ? {type: "symbol"} : symbol), (lexer.has("rparen") ? {type: "rparen"} : rparen)]},
    {"name": "math", "symbols": ["sum"]},
    {"name": "sum$subexpression$1", "symbols": [(lexer.has("plus") ? {type: "plus"} : plus)]},
    {"name": "sum$subexpression$1", "symbols": [(lexer.has("minus") ? {type: "minus"} : minus)]},
    {"name": "sum", "symbols": ["sum", "sum$subexpression$1", "product"]},
    {"name": "sum", "symbols": ["product"]},
    {"name": "product$subexpression$1", "symbols": [(lexer.has("multiply") ? {type: "multiply"} : multiply)]},
    {"name": "product$subexpression$1", "symbols": [(lexer.has("divide") ? {type: "divide"} : divide)]},
    {"name": "product", "symbols": ["product", "product$subexpression$1", "exp"]},
    {"name": "product", "symbols": ["exp"]},
    {"name": "exp", "symbols": ["number", (lexer.has("exp") ? {type: "exp"} : exp), "exp"]},
    {"name": "exp", "symbols": ["number"]},
    {"name": "number", "symbols": ["quantity"]},
    {"name": "number", "symbols": [(lexer.has("int") ? {type: "int"} : int)]},
    {"name": "number", "symbols": [(lexer.has("float") ? {type: "float"} : float)]},
    {"name": "quantity$subexpression$1", "symbols": [(lexer.has("int") ? {type: "int"} : int)]},
    {"name": "quantity$subexpression$1", "symbols": [(lexer.has("foat") ? {type: "foat"} : foat)]},
    {"name": "quantity", "symbols": ["quantity$subexpression$1", (lexer.has("symbol") ? {type: "symbol"} : symbol)]}
]
  , ParserStart: "main"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
})();
