function compile(str) {
    return 'console.log("aml")'
}

function check(str) {

}

if (!module.parent) {
    // parse compiler args
    var inputfile = process.argv[2];
    var outputfile = inputfile.replace('.aml', '')

    // tell the user what we are doing
    console.log(`Compiling ${inputfile}`)

    // do it
    const fs = require('fs')
    const js = compile(fs.readFileSync(inputfile))
    fs.writeFileSync(outputfile, '#!/usr/bin/env node\n' + js)
    fs.chmodSync(outputfile, 0o776)
    console.log(`Wrote executable ${outputfile}`)
}