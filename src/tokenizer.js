const moo = require('moo')

const fs = require('fs')

// in general these are keywords that specify attributes on a variable
// many of them can be used to inizialize a variable as well
// you know how sql works
const types = ('int,integer,long,i8,i16,i32,i64,i128,u8,u16,u32,u64,u128,'
  +'float,double,f8,f16,f32,f64,f128,'
  +'q8,q16,q32,q64,q128,'
  +'text,string,varchar,'
  +'date,datetime,'
  +'array,vector,'
  +'table,'
  +'not null,primary key,serial').split(',');

// these can be used to construct things
const initializers = 'var,const,table'.split(',')

// reserved keywords
const keyword_strings = 'sin,cos,tan,return,while,if,else,fn,for,import,as,from,not,nil,true,false,break,goto,do,end,repeat,until,then'.split(',')

const keywords = {
    type: types
}
keyword_strings.map(s => keywords[s] = s)

var lex_dict = {
  ws: /[ \t]+/,
  comment: /\/\/.*?$/,
  float: /[0-9]*?\.[0-9]*/,
  int: /[0-9]+/,
  string: /"(?:\\["\\"]|[^\n"\\"])*"/,
  lparen: '(',
  rparen: ')',
  lbracket: '[',
  rbracket: ']',
  lbrace: '{',
  rbrace: '}',
  comma: ',',
  dotdotdot: '...',
  dotdot: '..',
  dot: '.',
  coloncolon: '::',
  colon: ':',
  semicolon: ';',
  equals: '=',
  lt: '<',
  gt: '>',
  lte: '<=',
  gte: '>=',
  ne: '!=',
  eq: '==',
  plus: '+',
  minus: '-',
  multiply: '*',
  divide: '/',
  percent: '%',
  hash: '#',
  caret: '^',
  initializer: initializers,
  newline: { match: /\n/, lineBreaks: true },
  symbol: {
      match: /\w[a-zA-Z\d_]*/,
      type: moo.keywords(keywords)
    }
}

const _lexer = moo.compile(lex_dict)

module.exports.lexer = {
  next() {
    while (true) {
      var t = _lexer.next()
      if (!t || !['ws', 'comment', 'newline'].includes(t.type)) return t
    }
  },
  save() {
    return _lexer.save()
  },
  reset(chunk, info) {
    return _lexer.reset(chunk, info)
  },
  formatError(token) {
    return _lexer.formatError(token);
  },
  has(name) {
    return _lexer.has(name)
  }

}


// moo makes this really easy
module.exports.parse_file = function(fname) {
  console.log('parsing ' + fname)

  let lexer = module.exports.lexer;

  const file_string = fs.readFileSync(fname, 'utf8')
  console.log(file_string)

  lexer.reset(file_string)

  var t;
  var lexed = []
  var current_line = []
  var current_line_number = 1
  while (t = lexer.next()) {
    if (t.line > current_line_number) {
        lexed.push(current_line)
        current_line = []
        current_line_number = t.line
    }

    // ignore these types
    if (['ws', 'newline'].includes(t.type)) continue;

    current_line.push(`${t.value}|${t.type}`)
  }

  lexed.push(current_line)


  console.log(lexed.map(r => r.join(' ')).join('\n'))
}

if (!module.parent) {
    module.exports.parse_file(process.argv[2])
}