# wrote a tokenizer/lexer using moo.js
@{%
const lexer = require('./src/tokenizer').lexer
%}
@lexer lexer

# for now i'm just caring about basicall a script file, no functions or blocks or anything
main ->
  statementlist

statementlist ->
  statement:*

statement ->
  semicolon
  | (declaration | declareassign | expr) %semicolon

declaration ->
  definition

declareassign ->
  definition %equals expr

definition ->
  %type %symbol

expr ->
  ( math | call )

call ->
  %symbol %lparen %symbol %rparen


P ->
    %lparen AS %rparen {% d => d[1] %}
    | N {% id %}

E -> P %caret E {% d => Math.pow(d[0], d[2]) %}
    | P {% id %}

MD -> MD %times E {% d => d[0] * d[2] %}
    | MD %divide E {% d => d[0] / d[2] %}
    | E {% id %}

AS ->
    AS %plus MD {% (d) => d[0] + d[2] %}
    | AS %minus MD {% d => d[0] - d[2] %}
    | MD {% id %}

N ->
    %int {% d => parseInt(d[0]) %}
    | %float {% d => parseFloat(d[0]) %}
    | %symbol {% d => memory[d[0]] %}

quantity ->
  ( %int | %foat ) %symbol

semicolon -> %semicolon {% () => null %}